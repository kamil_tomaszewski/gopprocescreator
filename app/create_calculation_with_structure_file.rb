require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_insured'
require_relative 'action/select_product'
require_relative 'action/calculation'
require_relative 'action/get_bundle'

Auth.execute
bundle_id = CreateProcess.execute

SelectProduct.select_product(bundle_id, 'TRM')
SelectProduct.block_product(bundle_id)

SelectInsured.set_entitled(bundle_id)
SelectInsured.set_participants(bundle_id)
SelectInsured.set_structure(bundle_id)
SelectInsured.approve_select_insured(bundle_id)

main_rider_id = GetBundle.get_main_rider_id(bundle_id)
Calculation.set_rider_sum(bundle_id, main_rider_id)
