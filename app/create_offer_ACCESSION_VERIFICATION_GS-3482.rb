require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/proposal'
require_relative 'action/declaration'
require_relative 'action/get_bundle'
require_relative 'action/select_product'
require_relative 'action/rider'
require_relative 'action/underwriting'

Auth.execute
bundle_id = CreateProcess.execute

SelectProduct.block_product(bundle_id)

SelectInsured.set_entitled(bundle_id, 41)
SelectInsured.set_participants(bundle_id, 41, 41)
SelectInsured.set_structure_type(bundle_id)
SelectInsured.set_structure_gender(bundle_id, 31, 10)
SelectInsured.set_structure_average_age(bundle_id, 35)
SelectInsured.approve_select_insured(bundle_id)

scope_id = GetBundle.get_groups(bundle_id)[0]['scopes'][0]['id']
Calculation.add_variant(bundle_id, scope_id)
Calculation.select_variant_count(bundle_id, scope_id, 2)

variants = GetBundle.get_groups(bundle_id)[0]['scopes'][0]['variants']
Rider.fill_rider(bundle_id, variants, 'MAIN', [120_000, 110_000])
Rider.fill_rider(bundle_id, variants, 'CIB', [25_000, 30_000])
Rider.fill_rider(bundle_id, variants, 'BCB', [1_700, 1_800])

Calculation.financial_parameters(bundle_id, 0, "INKASO", 0,
                                 0, 0.09, 0, false)
Calculation.operation_commission(bundle_id, 17, 0.1)
Calculation.calculate_ape(bundle_id)
Calculation.send_to_acceptance(bundle_id)

bundle = GetBundle.get_bundle(bundle_id)
puts 'przeloguj na GOP_SUND identificationKey: ' + bundle['assigned']['identificationKey']
gets

uw_id = bundle['offer']['estimatedStructure']['underwriting']['id']
Underwriting.change_status(bundle_id, uw_id)
Calculation.set_calculation_status(bundle_id)

puts 'przeloguj na GOP_AGENT identificationKey: 10000146'
gets

Calculation.calculate_ape(bundle_id)
Calculation.calculate_participation(bundle_id)
Calculation.send_to_acceptance(bundle_id)

Offer.generate_offer(bundle_id)
Offer.submission_date(bundle_id)
Offer.acceptance(bundle_id)

holder_id = GetBundle.get_holder_id(bundle_id)
Proposal.holder_type(bundle_id, holder_id)
natural_person_id = GetBundle.get_natural_person_id(bundle_id)
Proposal.holder(bundle_id, holder_id, natural_person_id)
Proposal.holder_contract(bundle_id, holder_id)
Proposal.previous_insurer(bundle_id, holder_id)
Proposal.statements(bundle_id, holder_id)
Proposal.rcd(bundle_id)
Proposal.generate_proposal(bundle_id)

policy_number = GetBundle.get_policy_number(bundle_id)
#without differences
# (1..19).each do |_|
#   Declaration.send_declaration(policy_number, 1982, :K)
# end
# (1..2).each do |_|
#   Declaration.send_declaration(policy_number, 1991, :M)
# end
# (1..10).each do |_|
#   Declaration.send_declaration(policy_number, 1982, :K, 2)
# end
# (1..13).each do |_|
#   Declaration.send_declaration(policy_number, 1991, :M, 2)
# end
#
(1..21).each do |_|
  Declaration.send_declaration(policy_number, 1995, :K)
end
(1..10).each do |_|
  Declaration.send_declaration(policy_number, 1958, :K, 2)
end
(1..13).each do |_|
  Declaration.send_declaration(policy_number, 1991, :M, 2)
end

#with participation
# (1..5).each do |_|
#   Declaration.send_declaration(policy_number, 1958, :K)
# end
# (1..4).each do |_|
#   Declaration.send_declaration(policy_number, 1958, :M)
# end
# (1..3).each do |_|
#   Declaration.send_declaration(policy_number, 1999, :M)
# end
