require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/proposal'
require_relative 'action/get_bundle'

Auth.execute
bundle_id = CreateProcess.execute

SelectProduct.block_product(bundle_id)

SelectInsured.set_entitled(bundle_id)
SelectInsured.set_participants(bundle_id)
SelectInsured.set_structure(bundle_id)
SelectInsured.approve_select_insured(bundle_id)

main_rider_id = GetBundle.get_main_rider_id(bundle_id)
Calculation.set_rider_sum(bundle_id, main_rider_id)
Calculation.calculate_ape(bundle_id)
Calculation.send_to_acceptance(bundle_id)

Offer.generate_offer(bundle_id)
Offer.submission_date(bundle_id)
Offer.acceptance(bundle_id)

holder_id = GetBundle.get_holder_id(bundle_id)
natural_person_id = GetBundle.get_natural_person_id(bundle_id)
Proposal.holder_type(bundle_id, holder_id)
Proposal.holder(bundle_id, holder_id, natural_person_id)
Proposal.holder_contract(bundle_id, holder_id)
Proposal.previous_insurer(bundle_id, holder_id)
Proposal.statements(bundle_id, holder_id)
Proposal.rcd(bundle_id)
Proposal.generate_proposal(bundle_id)

