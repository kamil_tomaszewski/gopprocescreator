require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/proposal'
require_relative 'action/declaration'

policy_number = '4472'
(1..1).each do |_|
  Declaration.send_declaration(policy_number, 1920, :M, 1)
end
# (1..16).each do |_|
#   Declaration.send_declaration(policy_number, 1958, :K, 2)
# end
# (1..6).each do |_|
#   Declaration.send_declaration(policy_number, 1958, :M, 2)
# end

# with participation
# (1..5).each do |_|
#   Declaration.send_declaration(policy_number, 1958, :K)
# end
# (1..4).each do |_|
#   Declaration.send_declaration(policy_number, 1958, :M)
# end
# (1..3).each do |_|
#   Declaration.send_declaration(policy_number, 1999, :M)
# end
