require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/proposal'

Auth.execute
bundle_id = "63531"
main_rider_id = "26605763"

t1 = Time.now
Calculation.set_rider_sum(bundle_id, main_rider_id, 5000)
puts (Time.now - t1) * 1000.0