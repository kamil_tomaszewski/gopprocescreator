require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/proposal'
require_relative 'action/declaration'
require_relative 'action/get_bundle'
require_relative 'action/select_product'

Auth.execute
bundle_id = CreateProcess.execute

SelectProduct.select_product(bundle_id, 'TRM')
SelectProduct.block_product(bundle_id)

SelectInsured.set_entitled(bundle_id)
SelectInsured.set_participants(bundle_id)
SelectInsured.set_structure(bundle_id)
SelectInsured.approve_select_insured(bundle_id)

main_rider_id = GetBundle.get_main_rider_id(bundle_id)
Calculation.set_rider_sum(bundle_id, main_rider_id)
Calculation.calculate_ape(bundle_id)
Calculation.send_to_acceptance(bundle_id)

Offer.generate_offer_scopes(bundle_id)
Offer.submission_date(bundle_id)
Offer.acceptance(bundle_id)

holder_id = GetBundle.get_holder_id(bundle_id)
Proposal.holder_type(bundle_id, holder_id)
natural_person_id = GetBundle.get_natural_person_id(bundle_id)
Proposal.holder(bundle_id, holder_id, natural_person_id)
Proposal.holder_contract(bundle_id, holder_id)
Proposal.previous_insurer(bundle_id, holder_id)
Proposal.statements(bundle_id, holder_id)
Proposal.rcd(bundle_id)
Proposal.generate_proposal(bundle_id)

policy_number = GetBundle.get_policy_number(bundle_id)
#without differences
(1..16).each do |_|
  Declaration.send_declaration(policy_number, 1958, :K)
end
(1..4).each do |_|
  Declaration.send_declaration(policy_number, 1958, :M)
end

#with participation
# (1..5).each do |_|
#   Declaration.send_declaration(policy_number, 1958, :K)
# end
# (1..4).each do |_|
#   Declaration.send_declaration(policy_number, 1958, :M)
# end
# (1..3).each do |_|
#   Declaration.send_declaration(policy_number, 1999, :M)
# end
