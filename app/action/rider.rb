require_relative 'calculation'

class Rider

  def Rider.fill_rider(bundle_id, variants, rider_code, sums)
    i = 0
    sums.each do |sum|
      variants[i]['riders'].each {|rider|
        if rider['code'] == rider_code
          Calculation.change_select_rider(bundle_id, rider['id'])
          Calculation.set_rider_sum(bundle_id, rider['id'], sum)
        end
      }
      i += 1
    end
  end
end