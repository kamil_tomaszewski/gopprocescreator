require 'uri'
require 'json'
require_relative 'gop_response'

class SelectInsured

  def SelectInsured.set_entitled(bundle_id, entitled_count = 20)

    uri = "/#{bundle_id}/entitled/set-entitled-count"
    data = {"entitledCount": entitled_count}

    GopResponse.put(data, uri)
  end

  def SelectInsured.set_participants(bundle_id, entitled_count = 20, participants_count = 13)

    uri = "/#{bundle_id}/entitled/set-participants-count"
    data = {"entitledCount": entitled_count, "participantsCount": participants_count}

    GopResponse.put(data, uri)
  end

  def SelectInsured.set_structure(bundle_id, xls = "według_uprawnionych_staruchy.xlsx")

    uri = "/structure/#{bundle_id}"

    GopResponse.post_multipart("./action/data/" + xls, uri)
  end

  def SelectInsured.set_structure_type(bundle_id, structure_type = "ESTIMATED")

    uri = "/#{bundle_id}/entitled/change-structure-type"
    data = {"structureType": structure_type}

    GopResponse.put(data, uri)
  end

  def SelectInsured.set_structure_gender(bundle_id, female_quantity = 11, male_quantity = 9)

    uri = "/#{bundle_id}/entitled/estimated-structure/gender"
    data = {
        "femaleQuantity": female_quantity,
        "maleQuantity": male_quantity
    }

    GopResponse.put(data, uri)
  end

  def SelectInsured.change_upload_type(bundle_id, upload_type = "PARTICIPANTS")

    uri = "/#{bundle_id}/entitled/change-upload-type"

    data = {
        'uploadTypeCode': upload_type
    }

    GopResponse.put(data, uri)
  end

  def SelectInsured.set_structure_average_age(bundle_id, average_age = 55)

    uri = "/#{bundle_id}/entitled/estimated-structure/average-age"
    data = {"averageAge": average_age}

    GopResponse.put(data, uri)
  end

  def SelectInsured.approve_select_insured(bundle_id)

    uri = "/offer/approve-select-insured/#{bundle_id}"
    data = {}

    GopResponse.put(data, uri)
  end

  def SelectInsured.set_groups_quantity(bundle_id, quantity = "MULTIPLE")

    uri = "/#{bundle_id}/groups/quantity"
    data = {"quantity": quantity}

    GopResponse.post(data, uri)
  end

  def SelectInsured.set_group_name(bundle_id, group_id, name)

    uri = "/#{bundle_id}/groups"
    data = {"groupId": group_id, "name": name}

    GopResponse.put(data, uri)
  end

  def SelectInsured.add_group(bundle_id, name)

    uri = "/#{bundle_id}/groups"
    data = {"name": name}

    GopResponse.post(data, uri)
  end

end