require 'net/http'
require 'cgi/cookie'
require 'json'
require_relative 'gop_response'
require_relative 'hyperon_response'

class Auth

  def Auth.execute
    if ENV['xsrf']
      return
    end

    url = "/auth/user"
    response = GopResponse.get(url)

    cookies = response.response['set-cookie']
    xsrf = CGI::Cookie::parse(cookies)['XSRF-TOKEN'][0]
    ENV['xsrf'] = xsrf
  end


  def Auth.change_user(role, id)
    url = "/parameter/entry/update"
    data = {
        :parameterIdentifier => {
            :code => "userRole",
            :version => {
                :profile => "GROUP",
                :region => "",
                :version => "1"
            }
        },
        :sid => 123,
        :key => ['GOP'],
        :values => [["role", role]]
    }
    HyperonResponse.put(data, url)
  end

end
