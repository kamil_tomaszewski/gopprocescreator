require 'uri'
require 'json'
require_relative 'gop_response'

class Underwriting

  def Underwriting.change_status(bundle_id, underwriting_id, resolution = "RESOLVED")

    uri = "/underwriting/" + bundle_id
    data = {
        'underwritingId': underwriting_id,
        'resolution': resolution
    }

    GopResponse.post(data, uri)
  end
end