require 'uri'
require 'json'
require_relative 'gop_response'

class Calculation

  def Calculation.set_rider_sum(bundle_id, rider_id, sum = 130_000)

    uri = "/rider/#{bundle_id}/change-assured-sum"
    data = {:id => rider_id, :assuredSum => sum}

    GopResponse.put(data, uri)
  end

  def Calculation.select_rider(bundle_id, scope_id, rider_code, is_selected = true)

    uri = "/rider/#{bundle_id}/select"
    data = {:scopeId => scope_id, :riderCode => rider_code, :isSelected => is_selected}

    GopResponse.put(data, uri)
  end

  def Calculation.change_select_rider(bundle_id, rider_id, is_selected = true)

    uri = "/rider/#{bundle_id}/change-select"
    data = {:id => rider_id, :isSelected => is_selected}

    GopResponse.put(data, uri)
  end

  def Calculation.add_group_scope(bundle_id, group_id)

    uri = "/scope/#{bundle_id}"
    data = {:currentGroupId => group_id}

    GopResponse.post(data, uri)
  end

  def Calculation.add_offer_scope(bundle_id)

    uri = "/scope/#{bundle_id}/create-offer-scope"
    data = {}

    GopResponse.post(data, uri)
  end

  def Calculation.add_variant(bundle_id, scope_id)

    uri = "/#{bundle_id}/variant"
    data = {:scopeId => scope_id}

    GopResponse.post(data, uri)
  end

  def Calculation.select_variant_count(bundle_id, scope_id, selected_count)

    uri = "/scope/#{bundle_id}/select-variant-count"
    data = {
        :scopeId => scope_id,
        :selectedVariantsCount => selected_count
    }

    GopResponse.post(data, uri)
  end

  def Calculation.calculate_ape(bundle_id)

    uri = "/ape/#{bundle_id}"
    data = {}

    GopResponse.post(data, uri)
  end

  def Calculation.calculate_participation(bundle_id)

    uri = "/participation/#{bundle_id}"
    data = {}

    GopResponse.post(data, uri)
  end

  def Calculation.send_to_acceptance(bundle_id)

    uri = "/offer/send-to-acceptance/#{bundle_id}"
    data = {}

    GopResponse.put(data, uri)
  end

  def Calculation.set_calculation_status(bundle_id)

    uri = "/offer/set-calculation-status/#{bundle_id}"
    data = {}

    GopResponse.put(data, uri)
  end

  def Calculation.participation_uw(bundle_id, data)

    uri = "/participation/#{bundle_id}/underwriting"
    GopResponse.post(data, uri)
  end

  def Calculation.financial_parameters(bundle_id, commission = 0, commission_type = "INKASO", discount = 0,
      ma_discount = 0, premium_raise = 0, declaration_payment = 0, sponsored_discount = false)

    uri = "/financial-parameters/#{bundle_id}/save"
    data = {
        "commission": commission,
        "commissionType": commission_type,
        "discount": discount,
        "maDiscount": ma_discount,
        "premiumRaise": premium_raise,
        "declarationPayment": declaration_payment,
        "sponsoredDiscount": sponsored_discount
    }

    GopResponse.put(data, uri)
  end

  def Calculation.operation_commission(bundle_id, declaration = 0, commission = 0)

    uri = "/operation-commission/#{bundle_id}/save"
    data = {
        "declaration": declaration,
        "commission": commission,
    }

    GopResponse.put(data, uri)
  end

  def Calculation.rider_sum_max_limit(bundle_id, id, sum_max_limit_fixed, sum_max_limit_multiplicity)

    uri = "/rider-sum-max-limit/#{bundle_id}"
    data = {
        "riderSumMaxLimits": [
            "id": id,
            "sumMaxLimitFixed": sum_max_limit_fixed,
            "sumMaxLimitMultiplicity": sum_max_limit_multiplicity,
        ],
    }

    GopResponse.put(data, uri)
  end

  def Calculation.deviations(bundle_id, code)

    uri = "/deviation/#{bundle_id}/save-deviations"
    data = {
        "changedDeviations": [{"code": code, "isSelected": true}], "customDeviations": []
    }

    GopResponse.put(data, uri)
  end

end