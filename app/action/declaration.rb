require 'savon'
require 'json'
require_relative 'gop_response'
require_relative '../settings/settings'

class Declaration

  def Declaration.send_declaration(policy_number, birth_year, sex, subgroup = 1, worker = true)

    client = Savon.client(wsdl: 'http://localhost:8080/gop/services/declaration?wsdl', headers: {:SOAPAction => ''})

    # client = Savon.client(wsdl: 'https://u2gop.nn.pl/gop-webapp/declarations?wsdl', headers: {:SOAPAction => ''},
    #                       :ssl_verify_mode => :none,
    #                       :proxy => "http://10.81.128.99:8080")

    mesage_id = Random.new_seed
    accession_number = Random.new_seed

    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
      <soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
      <SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">
        <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" >
          <wsse:UsernameToken wsu:Id=\"UsernameToken-07ba5ea6-eb1a-43b6-bfbe-aedee1331af4\">
            <wsse:Username>soug_gop</wsse:Username>
            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">H%735pG12</wsse:Password>
          </wsse:UsernameToken>
        </wsse:Security>
      </SOAP-ENV:Header>
        <soap:Body>
          <ns2:createInsuredRequest xmlns:ns2=\"http://www.nn.pl/gop/api/ws/insgl/model/PolicyService/v1\">
            <messageId>
              <id>#{mesage_id}</id>
              <systemIdentifier>SOUG</systemIdentifier>
            </messageId>
            <policyNumber>#{policy_number}</policyNumber>
            <accessionNumber>#{accession_number}</accessionNumber>
            <birthYear>#{birth_year}</birthYear>
            <sex>#{sex}</sex>
            <insurerNIP>#{Settings::NIP}</insurerNIP>
            <subgroup>#{subgroup}</subgroup>
            <worker>#{worker}</worker>
          </ns2:createInsuredRequest>
       </soap:Body>
    </soap:Envelope>"

    client.call(:create_insured, xml: xml)

    # client.call(:create_insured, xml: xml,
    #             :ssl_verify_mode => :none,
    #             :proxy => "http://10.81.128.99:8080")

  end

end