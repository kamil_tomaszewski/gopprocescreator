require 'net/http'
require 'net/http/post/multipart'
require 'uri'
require 'json'


class HyperonResponse

  def HyperonResponse.get(url)

    response = Net::HTTP::get_response(get_uri_from_url(url))

    check_response(response)
    response
  end

  def HyperonResponse.put(data, url)

    uri = get_uri_from_url(url)

    response = client(uri).put(uri.path, data.to_json, get_headers)

    check_response(response, data)
    response
  end

  private

  def self.client(uri)
    Net::HTTP.new(uri.host, uri.port)
  end

  def self.check_response(response, data = '')
    puts response, data
    # raise 'incorrect return code \n' + response.body + '\n' + data if Integer(response.code) >= 300
  end

  def self.get_uri_from_url(url)
    URI.parse(api_url + url)
  end

  def self.api_url
    "http://localhost:38080/hyperon/api"
  end

  def self.get_headers
    {
        :Cookie => 'JSESSIONID=85327461909F13BA6381EDAE72D4ABBA',
    }
  end

end