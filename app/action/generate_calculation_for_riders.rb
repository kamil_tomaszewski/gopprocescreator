require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_product'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/get_bundle'

class GenerateCalculationForRiders

  def GenerateCalculationForRiders.vip(riders)
    Auth.execute
    bundle_id = CreateProcess.execute

    SelectProduct.select_product(bundle_id, 'VIP')
    SelectProduct.block_product(bundle_id)

    SelectInsured.set_entitled(bundle_id, 20)
    SelectInsured.set_participants(bundle_id, 20, 20)
    SelectInsured.set_structure(bundle_id, 'według_uprawnionych_staruchy.xlsx')

    select_riders(bundle_id, riders)
    fill_assured_sum(bundle_id)
    SelectInsured.approve_select_insured(bundle_id)
    main_rider_id = GetBundle.get_main_rider_id(bundle_id)
    Calculation.set_rider_sum(bundle_id, main_rider_id)

    bundle_id
  end

  def GenerateCalculationForRiders.trm(riders)
    Auth.execute
    bundle_id = CreateProcess.execute

    SelectProduct.select_product(bundle_id, 'TRM')
    SelectProduct.block_product(bundle_id)

    SelectInsured.set_entitled(bundle_id, 20)
    SelectInsured.set_participants(bundle_id, 20, 20)
    SelectInsured.set_structure(bundle_id, 'według_uprawnionych_staruchy.xlsx')

    select_riders(bundle_id, riders)
    fill_assured_sum(bundle_id)
    SelectInsured.approve_select_insured(bundle_id)
    main_rider_id = GetBundle.get_main_rider_id(bundle_id)
    Calculation.set_rider_sum(bundle_id, main_rider_id)

    bundle_id
  end

  def GenerateCalculationForRiders.sme(riders)
    Auth.execute
    bundle_id = CreateProcess.execute

    SelectProduct.block_product(bundle_id)

    SelectInsured.set_entitled(bundle_id, 16)
    SelectInsured.set_participants(bundle_id, 16, 16)
    SelectInsured.set_structure(bundle_id, 'według_uprawnionych_staruchy_sme.xlsx')

    select_riders(bundle_id, riders)
    fill_assured_sum(bundle_id)
    SelectInsured.approve_select_insured(bundle_id)
    main_rider_id = GetBundle.get_main_rider_id(bundle_id)
    Calculation.set_rider_sum(bundle_id, main_rider_id)

    bundle_id
  end

  # def GenerateCalculationForRiders.pac(riders)
  #   Auth.execute
  #   bundle_id = CreateProcess.execute
  #
  #   SelectProduct.select_product(bundle_id, 'PAC')
  #   SelectProduct.block_product(bundle_id)
  #
  #   SelectInsured.set_entitled(bundle_id, 19)
  #   SelectInsured.approve_select_insured(bundle_id)
  #
  #   main_rider_id = GetBundle.get_main_rider_id(bundle_id)
  #   Calculation.set_rider_sum(bundle_id, main_rider_id, 50_000)
  #
  #   bundle_id
  # end

  private

  def self.select_riders(bundle_id, riders)
    groups = GetBundle.get_groups(bundle_id)

    groups.each {|group|
      puts 'select riders for group ' + group['name']
      riders.each do |code|
        Calculation.select_rider(bundle_id, group["scopes"][0]["id"], code)
      end
    }
  end

  def self.fill_assured_sum(bundle_id)
    groups = GetBundle.get_groups(bundle_id)

    groups.each {|group|
      puts 'filla riders assured sum for group ' + group['name']

      group['scopes'].each {|scope|
        scope['variants'].each {|variant|
          variant['riders'].each {|rider|
            if rider['isSelected']
              id = rider['id']
              attributes = rider['riderAttributes']
              sum_max_fixed = attributes['sumMaxFixed']
              sum_min_fixed = attributes['sumMinFixed']
              round = ((sum_max_fixed + sum_min_fixed) / 2).ceil
              Calculation.set_rider_sum(bundle_id, id, round)
            end
          }
        }
      }
    }
  end
end
