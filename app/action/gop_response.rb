require 'net/http'
require 'net/http/post/multipart'
require 'uri'
require 'json'


class GopResponse

  def GopResponse.get(url)

    response = Net::HTTP::get_response(get_uri_from_url(url))

    get(url) if check_response(response)
    response
  end

  def GopResponse.get_file(url, filename)

    f = open("../download/#{filename}", "wb")
    begin
      Net::HTTP::get_response(get_uri_from_url(url)) do |resp|
        resp.read_body do |segment|
          f.write(segment)
        end
      end
    ensure
      f.close
    end
  end

  def GopResponse.put(data, url)

    uri = get_uri_from_url(url)

    response = client(uri).put(uri.path, data.to_json, get_headers)

    put(data, url) if check_response(response, data)
    response
  end

  def GopResponse.post(data, url)

    uri = get_uri_from_url(url)

    response = client(uri).post(uri.path, data.to_json, get_headers)

    post(data, url) if check_response(response, data)
    response
  end

  def GopResponse.post_multipart(file_path, url)

    uri = get_uri_from_url(url)


    content_type = "application/vnd.ms-excel"
    filename = "file.xlsx"

    File.open(file_path) do |xls|
      req = Net::HTTP::Post::Multipart.new uri.path,
                                           "file" => UploadIO.new(xls, content_type, filename)

      req.add_field('X-XSRF-TOKEN', get_xsrf)
      req.add_field(:Cookie, 'XSRF-TOKEN=' + get_xsrf)
      response = Net::HTTP.start(uri.host, uri.port) do |http|
        http.request(req)
      end

      post_multipart(file_path, url) if check_response(response)
    end
  end

  private

  def self.check_response(response, data = '')
    incorrect_code = Integer(response.code) >= 300
    puts "incorrect return code \n" + response.body + "\n" + data.to_s if incorrect_code
    r = 't'
    r = gets if incorrect_code
    return r.chomp != 't'
  end

  def self.client(uri)
    Net::HTTP.new(uri.host, uri.port)
  end

  def self.get_headers
    {
        'Content-Type' => 'application/json',
        'X-XSRF-TOKEN' => get_xsrf,
        :Cookie => 'XSRF-TOKEN=' + get_xsrf,
    }
  end

  def self.get_xsrf
    ENV['xsrf']
  end

  def self.get_uri_from_url(url)
    URI.parse(api_url + url)
  end

  def self.api_url
    api_host + "/gop/api"
  end

  def self.api_host
    "http://localhost:8080"
  end

end