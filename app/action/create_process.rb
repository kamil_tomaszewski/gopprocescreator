require 'net/http'
require 'uri'
require 'json'
require_relative 'gop_response'
require_relative '../settings/settings'
require_relative '../settings/channel_enum'
require_relative '../settings/user_enum'

class CreateProcess

  def CreateProcess.execute
    uri = "/offer"

    data = {
        :holders => [
            {
                :nip => Settings::NIP,
                :holderName => {
                    :value => 'SZYMKOWIAK GRUPA PRODUCENTÓW DROBIU SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚĆIĄ'
                },
                :pkd => {
                    :id => '1',
                    :excludedPkd => false,
                    :gusFetched => true,
                    :leadingPkd => true,
                    :value => '8510Z'
                },
                :address => {
                    :buildingNumber => '20',
                    :city => 'Dębienko',
                    :cityPost => 'Stęszew',
                    :flatNumber => '',
                    :postCode => '62-060',
                    :street => 'ul. Test-Wilcza'
                },
                :legalFormSpecialSymbol => '17',
                :main => true
            }
        ],
    }

    if Settings::CHANNEL == ChannelEnum::MA
      data[:owner] = nil
    end
    if Settings::USER == UserEnum::KAM
      data[:owner] = {
          "identificationKey": "10027901",
          "branchId": "636",
          "salesChannel": "MA",
          "tcSalesChannel": "IFA"
      }
    end

    response = GopResponse.post(data, uri)
    id = JSON.parse(response.body)['id']
    puts 'bundleId ' + id
    id
  end

end