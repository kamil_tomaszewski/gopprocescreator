require 'json'
require_relative 'gop_response'

class Proposal

  def Proposal.holder_type(bundle_id, holder_id, type = "NATURAL_PERSON")

    uri = "/#{bundle_id}/holder/#{holder_id}/type"
    data = {
        :type => type
    }

    GopResponse.put(data, uri)
  end

  def Proposal.holder(bundle_id, holder_id, natural_person_id)

    uri = "/#{bundle_id}/holder-aml/natural-persons"
    data = {
        "holderId": holder_id,
        "naturalPersonDataList": [
            {
                "id": natural_person_id,
                "birthplace": "PL",
                "citizenship": "PL",
                "documentType": "IDENTITY_CARD",
                "documentSerialNumber": 'TDB361692',
                "name": "KKK",
                "pesel": '64052358634',
                "surname": "EEE",
                "birthDate": "2019-02-17T23:00:00.000Z",
                "personalDataStatement": true,
                "healthStatement": true,
                "electronicMessageStatement": true,
                "voiceMessageStatement": true,
                "pepStatement": true
            },
        ]
    }

    GopResponse.put(data, uri)
  end

  def Proposal.holder_contract(bundle_id, holder_id)

    uri = "/#{bundle_id}/holder-contact/data"
    data = {
        :holderId => holder_id,
        :name => 'Roman',
        :entitledCount => 20,
        :surname => 'Kowalski',
        :holderContactId => '39780',
        :phoneNumber => '555444333',
        :statement => true,
        :emailAddress => 'rr@rr.pl'
    }

    GopResponse.put(data, uri)
  end

  def Proposal.previous_insurer(bundle_id, holder_id)

    uri = "/#{bundle_id}/previous-insurer"
    data = {
        :holderId => holder_id,
        :wasTakenFromPreviousInsurer => false
    }

    GopResponse.put(data, uri)
  end

  def Proposal.statements(bundle_id, holder_id)

    uri = "/#{bundle_id}/statements"
    data = {
        :holderId => holder_id,
        :electronicMessageStatement => true,
        :voiceMessageStatement => true,
        :emailStatement => true,
        :needsStatement => true,
        :infoStatement => true
    }

    GopResponse.put(data, uri)
  end

  def Proposal.rcd(bundle_id)

    uri = "/#{bundle_id}/rcd"
    data = {
        date: DateTime.now.strftime("%Y-%m-%d") + "T00:00:00+01:00",
        :statement => true
    }

    GopResponse.put(data, uri)
  end

  def Proposal.generate_proposal(bundle_id)

    uri = "/offer/set-proposal-status/#{bundle_id}"
    data = {}

    GopResponse.put(data, uri)
  end

end