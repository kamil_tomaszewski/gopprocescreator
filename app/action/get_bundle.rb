require 'uri'
require 'json'
require_relative 'gop_response'

class GetBundle

  def GetBundle.get_groups(bundle_id)

    get_offer(bundle_id)['groups']
  end

  def GetBundle.get_offer_scopes(bundle_id)

    get_offer(bundle_id)['offerScopes']
  end

  def GetBundle.get_main_rider_id(bundle_id)

    get_offer(bundle_id)['groups'][0]['scopes'][0]['variants'][0]['riders'][0]['id']
  end

  def GetBundle.get_holders(bundle_id)

    get_offer(bundle_id)['holderList']['value']
  end

  def GetBundle.get_holder_id(bundle_id)

    get_holders(bundle_id)[0]['id']
  end

  def GetBundle.get_natural_person_id(bundle_id)

    puts get_offer(bundle_id)['holderList']['value'][0]

    get_offer(bundle_id)['holderList']['value'][0]['naturalPersons'][0]['id']
  end

  def GetBundle.get_policy_number(bundle_id)

    get_offer(bundle_id)['policyNumber']
  end

  def GetBundle.get_participation_list(bundle_id)

    get_offer(bundle_id)['participationList']
  end

  def GetBundle.get_offer(bundle_id)

    get_bundle(bundle_id)['offer']
  end

  def GetBundle.get_bundle(bundle_id)

    url = "/bundle/#{bundle_id}"

    response = GopResponse.get(url)

    JSON.parse(response.body)
  end

  def GetBundle.save_all_pdf(bundle_id, filename)

    get_offer(bundle_id)['files'].each do |file|
      if file['extension'] == 'pdf'
        get_attachment(bundle_id, file['fileContentId'], "#{filename}_#{file['name']}")
      end
    end
  end

  def GetBundle.get_attachment(bundle_id, file_id, filename)

    uri = "/attachments/#{bundle_id}/#{file_id}"

    GopResponse.get_file(uri, filename)
  end

end