require 'uri'
require 'json'
require_relative 'gop_response'

class SelectProduct

  def SelectProduct.select_product(bundle_id, product_code = "SME")

    uri = "/product/#{bundle_id}/product-selection"
    data = {"productCode": product_code}

    GopResponse.put(data, uri)
  end

  def SelectProduct.block_product(bundle_id)

    uri = "/product/#{bundle_id}/block-product-selection"
    data = {}

    GopResponse.put(data, uri)
  end

end