require 'uri'
require 'json'
require 'date'
require_relative 'gop_response'

class Offer

  def Offer.generate_offer_scopes(bundle_id)

    uri = "/#{bundle_id}/printing/pdf/offer"
    data = {
        :offerView => 'SCOPES',
        :individualContinuationOffer => true,
        :showPremium => true,
        :showBenefitsOfEmployerAndEmployee => true,
        :showAwards => true,
        :showShortScope => true,
        :showAdditionalScopes => true,
        :generateDate => nil
    }

    GopResponse.post(data, uri)
  end

  def Offer.generate_offer_subgroups(bundle_id)

    uri = "/#{bundle_id}/printing/pdf/offer"
    data = {
        :offerView => 'SUBGROUPS',
        :individualContinuationOffer => true,
        :showPremium => true,
        :showBenefitsOfEmployerAndEmployee => true,
        :showAwards => true,
        :showShortScope => true,
        :showAdditionalScopes => true,
        :generateDate => nil
    }

    GopResponse.post(data, uri)
  end

  def Offer.submission_date(bundle_id)

    uri = "/offer/set-submission-date/#{bundle_id}"

    data = {:submissionDate => DateTime.now.strftime("%Y-%m-%d") + "T23:00:00.000Z"}

    GopResponse.post(data, uri)
  end

  def Offer.acceptance(bundle_id)

    uri = "/scope/#{bundle_id}/select-variants"

    data = {:selectedVariants => []}

    GopResponse.post(data, uri)
  end

end