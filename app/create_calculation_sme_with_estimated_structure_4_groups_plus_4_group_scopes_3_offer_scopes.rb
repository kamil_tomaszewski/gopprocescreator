require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_product'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/get_bundle'

Auth.execute
bundle_id = CreateProcess.execute

SelectProduct.select_product(bundle_id, "SME")

SelectInsured.set_entitled(bundle_id, 19)
SelectInsured.set_participants(bundle_id, 19, 13)

SelectInsured.set_groups_quantity(bundle_id)
groups = GetBundle.get_groups(bundle_id)
groups.each {|group|
  SelectInsured.set_group_name(bundle_id, group['id'], group['id'])
}
SelectInsured.add_group(bundle_id, "3")
SelectInsured.add_group(bundle_id, "4")
SelectInsured.add_group(bundle_id, "5")

SelectInsured.set_structure_type(bundle_id)
SelectInsured.set_structure_gender(bundle_id, 11, 8)
SelectInsured.set_structure_average_age(bundle_id)

groups = GetBundle.get_groups(bundle_id)

(1..3).each {|_|
  groups.each {|group|
    Calculation.add_group_scope(bundle_id, group['id'])
  }
}
(1..3).each {|_|
  Calculation.add_offer_scope(bundle_id)
}

groups = GetBundle.get_groups(bundle_id)

groups.each {|group|
  group["scopes"].each {|scope|
    puts 'add variant for scope ' + scope['name']

    (1..3).each {|_|
      Calculation.add_variant(bundle_id, scope["id"])
    }
  }
}

groups.each {|group|
  puts 'select riders for group ' + group['name']
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "ADR")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "ADT")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "WADR")

  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "HSDR")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "PDR")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "PDW")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "LHSD")

  Calculation.select_rider(bundle_id, group["scopes"][2]["id"], "MSR")
  Calculation.select_rider(bundle_id, group["scopes"][2]["id"], "HDB")
  Calculation.select_rider(bundle_id, group["scopes"][2]["id"], "HSHDB")

  Calculation.select_rider(bundle_id, group["scopes"][3]["id"], "STB")
  Calculation.select_rider(bundle_id, group["scopes"][3]["id"], "IPB")
  Calculation.select_rider(bundle_id, group["scopes"][3]["id"], "DSB")
}

offer_scopes = GetBundle.get_offer_scopes(bundle_id)


offer_scopes.each {|scope|
  puts 'add variant for offer scope ' + scope['name']

  (1..3).each {|_|
    Calculation.add_variant(bundle_id, scope["id"])
  }
}

puts 'select riders for offer scopes '

Calculation.select_rider(bundle_id, offer_scopes[0]["id"], "DPB")
Calculation.select_rider(bundle_id, offer_scopes[0]["id"], "ADPB")
Calculation.select_rider(bundle_id, offer_scopes[0]["id"], "BCB")

Calculation.select_rider(bundle_id, offer_scopes[1]["id"], "OCB")
Calculation.select_rider(bundle_id, offer_scopes[1]["id"], "SBB")
Calculation.select_rider(bundle_id, offer_scopes[1]["id"], "DCB")

Calculation.select_rider(bundle_id, offer_scopes[2]["id"], "ADR")
Calculation.select_rider(bundle_id, offer_scopes[2]["id"], "ADT")


SelectInsured.approve_select_insured(bundle_id)

main_rider_id = GetBundle.get_main_rider_id(bundle_id)
Calculation.set_rider_sum(bundle_id, main_rider_id)
