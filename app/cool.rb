require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/proposal'

Auth.execute
bundle_id = "20185000"
main_rider_id = "20186065"

(0..200).each {|i|
  puts i
  puts DateTime.now
  Calculation.set_rider_sum(bundle_id, main_rider_id, 120_000)
  t1 = Time.now
  Calculation.set_rider_sum(bundle_id, main_rider_id, 5000)
  puts (Time.now - t1) * 1000.0
  # Calculation.set_rider_sum(bundle_id, main_rider_id_2, 110_000)
  # Calculation.set_rider_sum(bundle_id, main_rider_id_2, 100_000)
}
puts DateTime.now
