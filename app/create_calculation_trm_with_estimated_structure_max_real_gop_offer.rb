require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_product'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/get_bundle'

# c.maksymalna kalkulacja: trzy grupy, zakres podstawowy z trzema wariantami dla każdej z grup,
# zakres dodatkowy z dwoma wariantami dla każdej z grup, zakres dodatkowy z jednym wariantem dla całej polisy,
# średnio 25 ryzyk w zakresie, zakres dla współubezpieczonych z trzema wariantami.

Auth.execute
bundle_id = CreateProcess.execute

SelectProduct.select_product(bundle_id, "TRM")
SelectProduct.block_product(bundle_id)

SelectInsured.set_entitled(bundle_id, 22)
SelectInsured.set_participants(bundle_id, 22, 22)

SelectInsured.set_groups_quantity(bundle_id)
groups = GetBundle.get_groups(bundle_id)
groups.each {|group|
  SelectInsured.set_group_name(bundle_id, group['id'], group['id'])
}
SelectInsured.add_group(bundle_id, "3")

SelectInsured.set_structure_type(bundle_id)
SelectInsured.set_structure_gender(bundle_id, 11, 11)
SelectInsured.set_structure_average_age(bundle_id)

groups = GetBundle.get_groups(bundle_id)

groups.each {|group|
  Calculation.add_group_scope(bundle_id, group['id'])
}

groups = GetBundle.get_groups(bundle_id)

groups.each {|group|
  group["scopes"].each {|scope|
    puts 'add variant for scope ' + scope['name']

    if scope["type"] == 'BASIC'
      (1..2).each {|_|
        Calculation.add_variant(bundle_id, scope["id"])
      }
    else
      Calculation.add_variant(bundle_id, scope["id"])
    end
  }
}

groups.each {|group|
  puts 'select riders for group ' + group['name']

  # 33 ryzyka
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "ADR")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "ADT")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "WADR")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "OCB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "SBB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "DCB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "MSR")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "HDB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "HSHDB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "STB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "IPB")

  # 36 ryzyk
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "MAIN")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "HSDR")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "PDR")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "PDW")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "LHSD")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "ADR")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "ADT")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "DSB")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "CIB")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "CCBH")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "MSR")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "HDB")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "HSHDB")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "AHDB")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "ATHDB")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "WAHDB")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "WAHDB")
  Calculation.select_rider(bundle_id, group["scopes"][1]["id"], "CDB")
}

groups = GetBundle.get_groups(bundle_id)

groups.each {|group|
  puts 'filla riders assured sum for group ' + group['name']

  group['scopes'].each {|scope|
    scope['variants'].each {|variant|
      variant['riders'].each {|rider|
        if rider['isSelected']
          id = rider['id']
          attributes = rider['riderAttributes']
          sum_max_fixed = attributes['sumMaxFixed']
          sum_min_fixed = attributes['sumMinFixed']
          Calculation.set_rider_sum(bundle_id, id, sum_min_fixed)
        end
      }
    }
  }
}


Calculation.add_offer_scope(bundle_id)
offer_scopes = GetBundle.get_offer_scopes(bundle_id)

puts 'select riders for offer scopes '

#7 ryzyk
Calculation.select_rider(bundle_id, offer_scopes[0]["id"], "DPB")
Calculation.select_rider(bundle_id, offer_scopes[0]["id"], "ADPB")
Calculation.select_rider(bundle_id, offer_scopes[0]["id"], "BCB")
Calculation.select_rider(bundle_id, offer_scopes[0]["id"], "DCB")
Calculation.select_rider(bundle_id, offer_scopes[0]["id"], "OCB")
Calculation.select_rider(bundle_id, offer_scopes[0]["id"], "SBB")


offer_scopes = GetBundle.get_offer_scopes(bundle_id)

offer_scopes.each {|scope|

  puts 'filla riders assured sum for offer scope ' + scope['name']

  scope['variants'].each {|variant|
    variant['riders'].each {|rider|
      if rider['isSelected']
        id = rider['id']
        attributes = rider['riderAttributes']
        sum_max_fixed = attributes['sumMaxFixed']
        sum_min_fixed = attributes['sumMinFixed']
        Calculation.set_rider_sum(bundle_id, id, sum_min_fixed)
      end
    }
  }
}


SelectInsured.approve_select_insured(bundle_id)

# main_rider_id = GetBundle.get_main_rider(bundle_id)
# Calculation.set_rider_sum(bundle_id, main_rider_id)
