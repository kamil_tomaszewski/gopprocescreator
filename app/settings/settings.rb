require_relative 'channel_enum'
require_relative 'user_enum'

module Settings

  USER = UserEnum::AGENT
  CHANNEL = ChannelEnum::TA
  NIP = '7773226052'
end
