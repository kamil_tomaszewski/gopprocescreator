require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_product'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/get_bundle'

# c.maksymalna kalkulacja: trzy grupy, zakres podstawowy z trzema wariantami dla każdej z grup,
# zakres dodatkowy z dwoma wariantami dla każdej z grup, zakres dodatkowy z jednym wariantem dla całej polisy,
# średnio 25 ryzyk w zakresie, zakres dla współubezpieczonych z trzema wariantami.

Auth.execute
bundle_id = CreateProcess.execute

# SelectProduct.select_product(bundle_id, "TRM")
SelectProduct.block_product(bundle_id)

SelectInsured.set_entitled(bundle_id, 16)
SelectInsured.set_participants(bundle_id, 16, 16)

SelectInsured.set_structure_type(bundle_id)
SelectInsured.set_structure_gender(bundle_id, 2, 14)
SelectInsured.set_structure_average_age(bundle_id)

groups = GetBundle.get_groups(bundle_id)


groups.each {|group|
  puts 'select riders for group ' + group['name']

  # 33 ryzyka
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "MAIN")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "ADR")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "ADT")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "WADR")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "HSDR")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "PDW")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "LHB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "LHSD")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "CIB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "HDB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "HSHDB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "AHDB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "ATHDB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "WAHDB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "MCR")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "DSB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "SADR")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "DPB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "ADPB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "BCB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "DCB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "OCB")
  Calculation.select_rider(bundle_id, group["scopes"][0]["id"], "SBB")
}

groups = GetBundle.get_groups(bundle_id)

groups.each {|group|
  puts 'filla riders assured sum for group ' + group['name']

  group['scopes'].each {|scope|
    scope['variants'].each {|variant|
      variant['riders'].each {|rider|
        if rider['isSelected']
          id = rider['id']
          attributes = rider['riderAttributes']
          sum_max_fixed = attributes['sumMaxFixed']
          sum_min_fixed = attributes['sumMinFixed']
          round = ((sum_max_fixed + sum_min_fixed) / 2).ceil
          Calculation.set_rider_sum(bundle_id, id, round)
        end
      }
    }
  }
}


SelectInsured.approve_select_insured(bundle_id)

# main_rider_id = GetBundle.get_main_rider(bundle_id)
# Calculation.set_rider_sum(bundle_id, main_rider_id)
