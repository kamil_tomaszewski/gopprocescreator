require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_product'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/get_bundle'
require_relative 'generate_calculation_for_riders'

class GenerateOfferForRiders

  def GenerateOfferForRiders.sme(riders)

    bundle_id = sme_acceptance(riders)
    Offer.generate_offer_scopes(bundle_id)

    bundle_id = sme_acceptance(riders)
    Offer.generate_offer_subgroups(bundle_id)
  end

  def GenerateOfferForRiders.trm(riders)

    bundle_id = trm_acceptance(riders)
    Offer.generate_offer_scopes(bundle_id)

    bundle_id = trm_acceptance(riders)
    Offer.generate_offer_subgroups(bundle_id)
  end

  def GenerateOfferForRiders.vip(riders)

    bundle_id = vip_acceptance(riders)
    #
    # Offer.generate_offer(bundle_id, true)
    bundle_id
  end

  private

  def self.vip_acceptance(riders)
    bundle_id = GenerateCalculationForRiders.vip(riders)

    Calculation.calculate_ape(bundle_id)
    Calculation.send_to_acceptance(bundle_id)
    bundle_id
  end

  def self.trm_acceptance(riders)
    bundle_id = GenerateCalculationForRiders.trm(riders)

    Calculation.calculate_ape(bundle_id)
    Calculation.send_to_acceptance(bundle_id)
    bundle_id
  end

  def self.sme_acceptance(riders)
    bundle_id = GenerateCalculationForRiders.sme(riders)

    Calculation.calculate_ape(bundle_id)
    Calculation.send_to_acceptance(bundle_id)
    bundle_id
  end
end
