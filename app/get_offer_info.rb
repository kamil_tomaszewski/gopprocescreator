require_relative 'action/auth'
require_relative 'action/get_bundle'
require 'json'

Auth.execute

bundle_id = '131000'
GetBundle.get_groups(bundle_id)[0]['scopes'][0]['variants'][0]['riders'].each do |rider|
  if rider['isSelected']
    puts "'" + rider['code'] + "'" + ", " + rider['assuredSum']['value'].to_s + ", " + rider['employerPremium']['value'].to_s
  end
end
