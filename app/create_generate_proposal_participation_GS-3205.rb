require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/get_bundle'
require_relative 'action/underwriting'
require_relative 'action/offer'
require_relative 'action/proposal'
require_relative 'action/declaration'

# 1. Jako GOP agent zarezerwuj NIP
# 2. TRm
# 3. Liczba uprawnionych 20
# 4. Liczba przystępujących 15
# 6. dwie grupy pracowników Zarząd i Pracownik
# 5.Struktura szczegółowa dla 15 osób z roku 1970: w grupie pracownicy 9 Mężczyzn, w grupie Zarząd 6 Kobiet
# Pracownicy:
#     MAIN 130 000,
# 6. Wariant w zakresie podstawowym :Zarząd
# MAIN 130 000, DSB 30 000;
#     Ustawienia => Prowizja agenta 15 %,
#                                  7 Oferta została przekazana do akceptacji UDW.. **UDW w ofercie:
# * - zaakceptował wiele grup
# * - zdefiniował partycypację dla każdej z grup ( oddzielnie A i oddzielnie B) na poziomie 40 %*
# 8. UDW odesłał ofertę do sprzedawcy
# 9 Agent użył przycisku wygeneruj wniosek


Auth.execute
bundle_id = CreateProcess.execute

SelectProduct.block_product(bundle_id)

SelectInsured.set_entitled(bundle_id, 20)
SelectInsured.set_participants(bundle_id, 20, 15)
SelectInsured.change_upload_type(bundle_id)

SelectInsured.set_groups_quantity(bundle_id)
groups = GetBundle.get_groups(bundle_id)
SelectInsured.set_group_name(bundle_id, groups[0]['id'], 'A')
SelectInsured.set_group_name(bundle_id, groups[1]['id'], 'B')

SelectInsured.set_structure(bundle_id, "GS-3057.xlsx")
SelectInsured.approve_select_insured(bundle_id)

groups = GetBundle.get_groups(bundle_id)

main_rider_id = GetBundle.get_main_rider_id(bundle_id)
Calculation.set_rider_sum(bundle_id, main_rider_id, 130_000)

groups[1]['scopes'][0]['variants'][0]['riders'].each {|rider|
  if rider['code'] == 'MAIN'
    Calculation.change_select_rider(bundle_id, rider['id'])
    Calculation.set_rider_sum(bundle_id, rider['id'], 130_000)
  end
  if rider['code'] == 'DSB'
    Calculation.change_select_rider(bundle_id, rider['id'])
    Calculation.set_rider_sum(bundle_id, rider['id'], 30_000)
  end
}

Calculation.calculate_ape(bundle_id)
Calculation.send_to_acceptance(bundle_id)
bundle = GetBundle.get_bundle(bundle_id)

puts 'przeloguj na GOP_SUND identificationKey: ' + bundle['assigned']['identificationKey']
gets

uw_id = bundle['offer']['groupsQuantity']['underwriting']['id']
Underwriting.change_status(bundle_id, uw_id)
group_id_a = groups[0]['id']
group_id_b = groups[1]['id']
scope_id_a = groups[0]['scopes'][0]['id']
scope_id_b = groups[1]['scopes'][0]['id']
data = {
    :useVariantParticipationLoading => false,
    :minimalParticipation => 65,
    :groupParticipations => [
        {'groupId': group_id_a, 'participation': 40},
        {'groupId': group_id_b, 'participation': 40}
    ],
    'participationRules': [
        {'scopeId': scope_id_a, 'variantParticipations': []},
        {'scopeId': scope_id_b, 'variantParticipations': []}
    ],
    'participationVariants': []
}
Calculation.participation_uw(bundle_id, data)
Calculation.set_calculation_status(bundle_id)

puts 'przeloguj na GOP_AGENT identificationKey: 10000146'
gets

Calculation.calculate_ape(bundle_id)
Calculation.send_to_acceptance(bundle_id)

Offer.generate_offer(bundle_id)
Offer.submission_date(bundle_id)
Offer.acceptance(bundle_id)

holder_id = GetBundle.get_holder_id(bundle_id)
natural_person_id = GetBundle.get_natural_person_id(bundle_id)
Proposal.holder_type(bundle_id, holder_id)
Proposal.holder(bundle_id, holder_id, natural_person_id)
Proposal.holder_contract(bundle_id, holder_id)
Proposal.previous_insurer(bundle_id, holder_id)
Proposal.statements(bundle_id, holder_id)
Proposal.rcd(bundle_id)
Proposal.generate_proposal(bundle_id)

policy_number = GetBundle.get_policy_number(bundle_id)
(1..8).each do |_|
  Declaration.send_declaration(policy_number, 2000, :K, 1)
end
(1..6).each do |_|
  Declaration.send_declaration(policy_number, 1948, :M, 2)
end
