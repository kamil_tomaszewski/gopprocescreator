require_relative 'generate_calculation_for_riders'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/proposal'

trm_without_deviations = []
trm_with_szpit1 = []
trm_with_wiekdziec = []
trm_with_smdziecwiek = []
trm_with_l4 = []
trm_with_sporekst = []
trm_with_preex = []
trm_with_partnerdef = []

vip_without_deviations = []
vip_with_szpit1 = []
vip_with_cc = []
vip_with_borel = []
vip_with_suic = []

threads = []
Auth.execute

threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_1
  trm_without_deviations.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_2
  trm_without_deviations.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_3
  trm_without_deviations.push(bundle_id)
}

threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_1
  Calculation.deviations(bundle_id, 'SZPIT1')
  trm_with_szpit1.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_2
  Calculation.deviations(bundle_id, 'SZPIT1')
  trm_with_szpit1.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_3
  Calculation.deviations(bundle_id, 'SZPIT1')
  trm_with_szpit1.push(bundle_id)
}

threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_1
  Calculation.deviations(bundle_id, 'WIEKDZIEC')
  trm_with_wiekdziec.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_1
  Calculation.deviations(bundle_id, 'SMDZIECWIEK')
  trm_with_smdziecwiek.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_1
  Calculation.deviations(bundle_id, 'L4')
  trm_with_l4.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_1
  Calculation.deviations(bundle_id, 'SPOREKST')
  trm_with_sporekst.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_1
  Calculation.deviations(bundle_id, 'PREEX')
  trm_with_preex.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.trm_all_riders_1
  Calculation.deviations(bundle_id, 'PARTNERDEF')
  trm_with_partnerdef.push(bundle_id)
}


threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.vip_all_riders_1
  vip_without_deviations.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.vip_all_riders_2
  vip_without_deviations.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.vip_all_riders_3
  vip_without_deviations.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.vip_all_riders_1
  Calculation.deviations(bundle_id, 'SZPIT1')
  vip_with_szpit1.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.vip_all_riders_2
  Calculation.deviations(bundle_id, 'SZPIT1')
  vip_with_szpit1.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.vip_all_riders_3
  Calculation.deviations(bundle_id, 'SZPIT1')
  vip_with_szpit1.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.vip_all_riders_1
  Calculation.deviations(bundle_id, 'CC')
  vip_with_cc.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.vip_all_riders_1
  Calculation.deviations(bundle_id, 'BOREL')
  vip_with_borel.push(bundle_id)
}
threads.push Thread.new {
  bundle_id = GenerateCalculationForRiders.vip_all_riders_1
  Calculation.deviations(bundle_id, 'SUIC')
  vip_with_suic.push(bundle_id)
}

threads.each(&:join)


all_trm = trm_without_deviations + trm_with_szpit1 + trm_with_wiekdziec + trm_with_smdziecwiek + trm_with_l4 + trm_with_sporekst + trm_with_preex + trm_with_partnerdef
all_vip = vip_without_deviations + vip_with_szpit1 + vip_with_cc + vip_with_borel + vip_with_suic
all = all_trm + all_vip

all.each do |id|
  Calculation.calculate_ape(id)
  Calculation.send_to_acceptance(id)
end

bundle = GetBundle.get_bundle(all[0])
puts 'przeloguj na GOP_JUND identificationKey: ' + bundle['assigned']['identificationKey']
gets

all_vip.each do |id|
  Calculation.send_to_acceptance(id)
end
bundle = GetBundle.get_bundle(all[0])

puts 'przeloguj na GOP_AGENT identificationKey: ' + bundle['assigned']['identificationKey']
gets

threads = []
all_vip.each do |id|
  threads.push Thread.new {
    Calculation.send_to_acceptance(id)
  }
end
threads.each(&:join)

threads = []
all.each do |id|
  threads.push Thread.new {
    Offer.generate_offer_scopes(id)
  }
end
threads.each(&:join)

threads = []
threads.push Thread.new {
  trm_without_deviations.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "trm_#{index}_bez_odstępstw")
  end
}
threads.push Thread.new {
  trm_with_szpit1.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "trm_#{index}_odstępstwo_szpital")
  end
}
threads.push Thread.new {
  trm_with_wiekdziec.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "trm_#{index}_odstępstwo_wiekdziec")
  end
}
threads.push Thread.new {
  trm_with_smdziecwiek.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "trm_#{index}_odstępstwo_smdziecwiek")
  end
}
threads.push Thread.new {
  trm_with_l4.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "trm_#{index}_odstępstwo_l4")
  end
}
threads.push Thread.new {
  trm_with_sporekst.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "trm_#{index}_odstępstwo_sporekst")
  end
}
threads.push Thread.new {
  trm_with_preex.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "trm_#{index}_odstępstwo_preex")
  end
}
threads.push Thread.new {
  trm_with_partnerdef.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "trm_#{index}_odstępstwo_partnerdef")
  end
}
threads.push Thread.new {
  vip_without_deviations.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "vip_#{index}_bez_odstępstw")
  end
}
threads.push Thread.new {
  vip_with_szpit1.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "vip_#{index}_odstępstwo_szpital")
  end
}
threads.push Thread.new {
  vip_with_cc.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "vip_#{index}_odstępstwo_cc")
  end
}
threads.push Thread.new {
  vip_with_borel.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "vip_#{index}_odstępstwo_borel")
  end
}
threads.push Thread.new {
  vip_with_suic.each_with_index do |id, index|
    GetBundle.save_all_pdf(id, "vip_#{index}_odstępstwo_suic")
  end
}
threads.each(&:join)

# threads = []
# all.each do |id|
#   threads.push Thread.new {
#     Offer.submission_date(id)
#     # Offer.acceptance(id)
#     #
#     # holders = GetBundle.get_holders(id)
#     #
#     # holders.each do |holder|
#     #
#     #   holder_id = holder['id']
#     #   Proposal.holder_type(id, holder_id)
#     #   natural_person_id = GetBundle.get_natural_person_id(id)
#     #   Proposal.holder(id, holder_id, natural_person_id)
#     #   Proposal.holder_contract(id, holder_id)
#     #   Proposal.previous_insurer(id, holder_id)
#     #   Proposal.statements(id, holder_id)
#     #
#     # end
#     #
#     # Proposal.rcd(id)
#     # Proposal.generate_proposal(id)
#   }
# end
# threads.each(&:join)

