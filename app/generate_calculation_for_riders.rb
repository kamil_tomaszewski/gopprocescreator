require_relative 'action/auth'
require_relative 'action/create_process'
require_relative 'action/select_product'
require_relative 'action/select_insured'
require_relative 'action/calculation'
require_relative 'action/offer'
require_relative 'action/get_bundle'

class GenerateCalculationForRiders

  def GenerateCalculationForRiders.sme_all_riders_1
    sme(%w(ADR ADT WADR HSDR PDR PDW LHSD CIB CCB CIB_D MSR HDB HSHDB AHDB ATHDB WAHDB CDB
SSB STB MCR IPB DSB SADR SADT SLHB DPB ADPB BCB DCB OCB SBB SCIB SCCB SCIB_D SHDB SAHDB CCIB CHDB CAHDB))
  end

  def GenerateCalculationForRiders.sme_all_riders_2
    sme(%w(ADR ADT WADR HSDR PDR+ LHB LHSD TPD CIB CIB_D MSR HCB HSHDB AHDB ATHDB WAHDB CDB
SSB STB MCR IPB DSB SADR SADT SLHB DPB ADPB BCB DCB OCB SBB SCIB SCCB SCIB_D SHDB SAHDB CCIB CHDB CAHDB CLHB))
  end

  def GenerateCalculationForRiders.sme_all_riders_3
    sme(%w(ADR ADT WADR HSDR PDR+ LHBH LHSD TPDT CIB CCBH CIB_D MSR HDBH HSHDB AHDB ATHDB
WAHDB CDB SSB STB MCR IPB DSB SADR SADT SLHB DPB ADPB BCB DCB OCB SBB SCIB SCCB SCIB_D SHDB SAHDB CCIB CHDB CAHDB))
  end

  def GenerateCalculationForRiders.trm_all_riders_1
    trm(%w(ADR ADT WADR HSDR PDR PDW LHSD CIB CCB CIB_D MSR HDB HSHDB AHDB ATHDB WAHDB CDB SSB
 STB MCR IPB DSB SADR SADT SLHB DPB ADPB BCB DCB OCB SBB SCIB SCCB SCIB_D SHDB SAHDB CCIB CHDB CAHDB))
  end

  def GenerateCalculationForRiders.trm_all_riders_2
    trm(%w(ADR ADT WADR HSDR PDR+ LHB LHSD TPD CIB CIB_D MSR HCB HSHDB AHDB ATHDB WAHDB CDB
SSB STB MCR IPB DSB SADR SADT SLHB DPB ADPB BCB DCB OCB SBB SCIB SCCB SCIB_D SHDB SAHDB CCIB CHDB CAHDB CLHB))
  end

  def GenerateCalculationForRiders.trm_all_riders_3
    trm(%w(ADR ADT WADR HSDR PDR+ LHBH LHSD TPDT CIB CCBH CIB_D MSR HDBH HSHDB AHDB ATHDB
WAHDB CDB SSB STB MCR IPB DSB SADR SADT SLHB DPB ADPB BCB DCB OCB SBB SCIB SCCB SCIB_D SHDB SAHDB CCIB CHDB CAHDB))
  end

  def GenerateCalculationForRiders.vip_all_riders_1
    vip(%w(ADR ADT WADR HSDR PDR PDW LHSD CIB CCB CIB_D MSR HDB HSHDB AHDB ATHDB WAHDB CDB SSB STB MCR IPB))
  end

  def GenerateCalculationForRiders.vip_all_riders_2
    vip(%w(ADR ADT WADR HSDR PDR+ LHB TPD CIB CCBH CIB_D MSR HCB HSHDB AHDB ATHDB WAHDB CDB SSB STB MCR IPB))
  end

  def GenerateCalculationForRiders.vip_all_riders_3
    vip(%w(ADR ADT WADR HSDR PDR+ LHBH TPDT CIB CCBH CIB_D MSR HDBH HSHDB AHDB ATHDB WAHDB CDB STB MCR IPB))
  end


  def GenerateCalculationForRiders.vip(riders)
    Auth.execute
    bundle_id = CreateProcess.execute

    SelectProduct.select_product(bundle_id, 'VIP')
    SelectProduct.block_product(bundle_id)

    SelectInsured.set_entitled(bundle_id, 20)
    SelectInsured.set_participants(bundle_id, 20, 20)
    SelectInsured.set_structure(bundle_id, 'według_uprawnionych_staruchy.xlsx')

    select_riders(bundle_id, riders)
    fill_assured_sum(bundle_id)
    SelectInsured.approve_select_insured(bundle_id)
    main_rider_id = GetBundle.get_main_rider_id(bundle_id)
    Calculation.set_rider_sum(bundle_id, main_rider_id)

    bundle_id
  end

  def GenerateCalculationForRiders.trm(riders)
    Auth.execute
    bundle_id = CreateProcess.execute

    SelectProduct.select_product(bundle_id, 'TRM')
    SelectProduct.block_product(bundle_id)

    SelectInsured.set_entitled(bundle_id, 20)
    SelectInsured.set_participants(bundle_id, 20, 20)
    SelectInsured.set_structure(bundle_id, 'według_uprawnionych_staruchy.xlsx')

    select_riders(bundle_id, riders)
    fill_assured_sum(bundle_id)
    SelectInsured.approve_select_insured(bundle_id)
    main_rider_id = GetBundle.get_main_rider_id(bundle_id)
    Calculation.set_rider_sum(bundle_id, main_rider_id)

    bundle_id
  end

  def GenerateCalculationForRiders.sme(riders)
    Auth.execute
    bundle_id = CreateProcess.execute

    SelectProduct.block_product(bundle_id)

    SelectInsured.set_entitled(bundle_id, 16)
    SelectInsured.set_participants(bundle_id, 16, 16)
    SelectInsured.set_structure(bundle_id, 'według_uprawnionych_staruchy_sme.xlsx')

    select_riders(bundle_id, riders)
    fill_assured_sum(bundle_id)
    SelectInsured.approve_select_insured(bundle_id)
    main_rider_id = GetBundle.get_main_rider_id(bundle_id)
    Calculation.set_rider_sum(bundle_id, main_rider_id)

    bundle_id
  end

  private

  def self.select_riders(bundle_id, riders)
    groups = GetBundle.get_groups(bundle_id)

    groups.each {|group|
      puts 'select riders for group ' + group['name']
      riders.each do |code|
        Calculation.select_rider(bundle_id, group["scopes"][0]["id"], code)
      end
    }
  end

  def self.fill_assured_sum(bundle_id)
    groups = GetBundle.get_groups(bundle_id)

    groups.each {|group|
      puts 'filla riders assured sum for group ' + group['name']

      group['scopes'].each {|scope|
        scope['variants'].each {|variant|
          variant['riders'].each {|rider|
            if rider['isSelected']
              id = rider['id']
              attributes = rider['riderAttributes']
              sum_max_fixed = attributes['sumMaxFixed']
              sum_min_fixed = attributes['sumMinFixed']
              round = ((sum_max_fixed + sum_min_fixed) / 2).ceil
              Calculation.set_rider_sum(bundle_id, id, round)
            end
          }
        }
      }
    }
  end
end
